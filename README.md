# MySQL Grant Auditor

## About

Lists grants for all users on a MySQL server.

## Usage

```sh
./mysqlaudit.sh
```

## Example

```sh
./mysqlaudit.sh
Enter host: localhost
Enter mysql options:
Enter user: root
Enter password:

# ''@'localhost'
GRANT USAGE ON *.* TO ''@'localhost'
# 'root'@'127.0.0.1'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' WITH GRANT OPTION
# 'root'@'::1'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'::1' WITH GRANT OPTION
# 'root'@'localhost'
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION
GRANT PROXY ON ''@'%' TO 'root'@'localhost' WITH GRANT OPTION
```


## Thanks

* This is adapted from a comment by Sylvain Viart posted on
  [October 11, 2006](https://dev.mysql.com/doc/refman/5.7/en/show-grants.html).

